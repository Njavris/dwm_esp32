#ifndef __DWM_IF_H__
#define __DWM_IF_H__

#if defined(CONFIG_DWM_SPI)
#include "driver/spi_master.h"
#elif defined(CONFIG_DWM_UART)
#include "driver/uart.h"
#endif

#if defined(CONFIG_DWM_SPI)
struct spi_dev {
    spi_device_handle_t spidev_hndl;
    void (*tx_rx)(struct spi_dev *, uint8_t *, uint8_t *, uint32_t);
};
#elif defined(CONFIG_DWM_UART)
struct uart_dev {
    int uart_num;
    void (*tx_rx)(struct uart_dev *, uint8_t *, uint8_t *, uint32_t);
}; 
#endif

struct TLV {
    uint8_t type;
    uint8_t length;
    uint8_t payload[0];
} __attribute__((packed));

struct dwm_if_dev {
#if defined(CONFIG_DWM_SPI)
    struct spi_dev spi;
#elif defined(CONFIG_DWM_UART)
    struct uart_dev uart;
#endif
    int (*tlv_msg)(struct dwm_if_dev *, struct TLV *, uint8_t *, int); 
};

void dwm_if_init(struct dwm_if_dev *dev);
#endif
