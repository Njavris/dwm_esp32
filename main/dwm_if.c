#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "sdkconfig.h"
#include "dwm_if.h"
#include "esp_log.h"

#if defined(CONFIG_DWM_SPI)
static void spi_tx_rx(struct spi_dev *dev, uint8_t *tx_data,
		uint8_t *rx_data, uint32_t sz) {
    spi_transaction_t trans;
    memset(&trans, 0, sizeof(spi_transaction_t));
    trans.length = sz << 3;
    if (sz <= 4) {
	if (tx_data) {
	    trans.flags |= SPI_TRANS_USE_TXDATA;
	    memcpy(trans.tx_data, tx_data, sz);
	}
	if (rx_data) {
	    trans.flags |= SPI_TRANS_USE_RXDATA;
	    trans.rxlength = sz << 3;
	}
    } else {
	trans.tx_buffer = tx_data;
	trans.rx_buffer = rx_data;
    }
    ESP_ERROR_CHECK(spi_device_transmit(dev->spidev_hndl, &trans));
    if (sz <= 4 && rx_data)
	memcpy(rx_data, &trans.rx_data, sz);
}

void spi_init(struct spi_dev *dev) {
    memset(dev, 0, sizeof(struct spi_dev));
    spi_bus_config_t buscfg = {
	.miso_io_num = CONFIG_DWM_SPI_PIN_MISO,
	.mosi_io_num = CONFIG_DWM_SPI_PIN_MOSI,
	.sclk_io_num = CONFIG_DWM_SPI_PIN_CLK,
	.quadwp_io_num = -1,
	.quadhd_io_num = -1,
	.max_transfer_sz = 0,
    };
    spi_device_interface_config_t spidev = {
	.clock_speed_hz = CONFIG_DWM_SPI_SPEED,
	.spics_io_num = CONFIG_DWM_SPI_PIN_CS,
	.mode = 0,
	.cs_ena_pretrans = 0,
	.cs_ena_posttrans = 0,
	.command_bits = 0,
	.address_bits = 0,
	.dummy_bits = 0,
	.queue_size = 4,
	.flags = 0,
	.duty_cycle_pos = 0,
	.input_delay_ns = 0,
	.pre_cb = NULL,
	.post_cb = NULL,
    };
    ESP_ERROR_CHECK(spi_bus_initialize(VSPI_HOST, &buscfg, SPI_DMA_CH_AUTO));
    ESP_ERROR_CHECK(spi_bus_add_device(VSPI_HOST, &spidev, &dev->spidev_hndl));

    dev->tx_rx = spi_tx_rx;
}
#elif defined(CONFIG_DWM_UART)
static void uart_tx_rx(struct uart_dev *dev, uint8_t *tx_data,
		uint8_t *rx_data, uint32_t sz) {
    uint8_t buf[0x100];
    int ret = 0;
    if (sz > 0x100)
	return;
    if (tx_data) {
	uart_write_bytes(dev->uart_num, tx_data, sz);
    }
    if (rx_data) {
	ret = uart_read_bytes(dev->uart_num, buf, sizeof(buf),
		1000/portTICK_RATE_MS);
    }
    if (ret > sz)
	ESP_LOGE(__func__, "Received %d > %d\n", ret, sz);
    if (rx_data)
	memcpy(rx_data, buf, ret < sz ? ret : sz);
}

void uart_init(struct uart_dev *dev) {
    dev->uart_num = CONFIG_DWM_UART_PORT_NR;
    const uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };
    uart_driver_install(dev->uart_num, 0x100, 0, 0, NULL, 0);
    uart_param_config(dev->uart_num, &uart_config);
    uart_set_pin(dev->uart_num, CONFIG_DWM_UART_PIN_TX,
	CONFIG_DWM_UART_PIN_RX, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    dev->tx_rx = uart_tx_rx;
}
#endif



static int tlv_send(struct dwm_if_dev *dev, struct TLV *tlv,
				uint8_t *resp, int rlen){ 
    uint8_t sznm[] = {0, 0};
    if (!tlv)
	return -1;
#if defined(CONFIG_DWM_SPI)
    struct spi_dev *ifdev = &dev->spi;
#elif defined(CONFIG_DWM_UART)
    struct uart_dev *ifdev = &dev->uart;
#endif
    ifdev->tx_rx(ifdev, tlv, NULL, sizeof(struct TLV) + tlv->length);

    while (sznm[0] == 0 && sznm[1] == 0)
	ifdev->tx_rx(ifdev, NULL, &sznm, sizeof(resp));

    if ((sznm[0] * sznm[1]) > rlen) {
	ESP_LOGE(__func__, "buffer too small\n");
	return -1;
    }

    for (int i = 0; i < sznm[1]; i++) {
	int off = i * sznm[0];
	ifdev->tx_rx(ifdev, NULL, &resp[off], sznm[0]);
    }
    return sznm[0] * sznm[1];
}

void dwm_if_init(struct dwm_if_dev *dev) {
#if defined(CONFIG_DWM_SPI)
    spi_init(&dev->spi);
#elif defined(CONFIG_DWM_UART)
    uart_init(&dev->uart);
#endif
    dev->tlv_msg = tlv_send;
}
